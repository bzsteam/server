/**
 * Created by aditya on 08/04/17.
 */

let {Aggregate} = require('../models/aggregate');
let logger = require('../logger');


module.exports.update = function(apiKey, data) {

    Aggregate.update({apiKey: apiKey}, {$set: data}, {upsert: true, setDefaultsOnInsert: true}, function (err) {
        if(err)
            logger.log(`Updater error ${apiKey}`, err);
        logger.log(`Updating ${apiKey}`,Object.keys(data)[0]);
    });

};