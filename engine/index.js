/**
 * Created by aditya on 07/04/17.
 */


let cron = require('node-cron');

let live = require('./live');
let historic = require('./historic');
let logger = require('../logger');
const cluster = require('cluster');


let {User, ObjectId} = require('../models/user');


if(cluster.isWorker && cluster.worker.id === 1){
    logger.log(`Aggregator cluster:${cluster.worker.id} ${process.pid}`);

    let task = cron.schedule('*/10 * * * * *', function() {
        logger.log(`Aggregator cluster:${cluster.worker.id} ${process.pid} will execute every 30 secs`);

        User.find({}, '_id', function (err, docs) {
            // logger.log("err", err);
            //logger.log("docs", docs);
            if (docs)
                for (let doc of docs) {
                    let apiKey = doc._id;
                    live.countLiveUser(apiKey);
                    historic.usersOverTime(apiKey);
                    historic.pageVisitCount(apiKey);
                    historic.newVsReturningUsers(apiKey);
                    historic.avgPageLoadTime(apiKey);
                    historic.referrals(apiKey);
                    historic.eventCount(apiKey);
                    historic.osSpectrum(apiKey);
                    historic.languageSpectrum(apiKey);
                    historic.mobileSpectrum(apiKey);
                    historic.geoSpectrum(apiKey);
                }
        });
    });
}




//live.countLiveUser();
//historic.usersOverTime();
//historic.geoSpectrum();

/*let task = cron.schedule('*!/10 * * * * *', function() {
 logger.log(`${process.pid} will execute every 1 min until stopped`);
 //live.countLiveUser();
 });*/
//task.start();