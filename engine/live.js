/**
 * Created by aditya on 07/04/17.
 */


let {User, ObjectId} = require('../models/user');
let {Event} = require('../models/event');

let Updater = require('./updater');
let logger = require('../logger');


module.exports.countLiveUser = function (apiKey) {
    Event.aggregate([
        {$match: {'dateCreated': {'$gte': new Date(Date.now() - 1000 * 60 * 30)}}},
        {$group: {'_id': '$eventStory.guid'}}
    ], function (err, docs) {
        //logger.log("err", err);
        //logger.log("docs", docs.length);

        let tmp = {countLiveUser: docs.length};
        Updater.update(apiKey, {live: tmp});

    });
};
