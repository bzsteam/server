/**
 * Created by aditya on 07/04/17.
 */


let {User, ObjectId} = require('../models/user');
let {Event} = require('../models/event');

let Updater = require('./updater');
let logger = require('../logger');


module.exports.usersOverTime = function (apiKey) {

    Event.aggregate([
        {$group: {'_id': {date: '$dateCreated', 'guid': '$eventStory.guid.guid'}, 'count': {$sum: 1}}}
    ], function (err, docs) {
        //logger.log("err", err);
        //logger.log("docs", docs);

        //let tmp = {usersOverTime: docs};
        Updater.update(apiKey, {'historic.usersOverTime': docs});
    });
};


module.exports.pageVisitCount = function (apiKey) {

    Event.aggregate([
        {$group: {'_id': {url: '$eventStory.page.url', pageTitle: '$eventStory.page.pageTitle'}, 'count': {$sum: 1}}}
    ], function (err, docs) {
        //logger.log("err", err);
        //logger.log("docs", docs);

        //let tmp = {pageVisitCount: docs};
        Updater.update(apiKey, {'historic.pageVisitCount': docs});
    });
};


module.exports.newVsReturningUsers = function (apiKey) {

    Event.aggregate([
        {$group: {'_id': '$eventStory.guid', count: {$sum: 1}}}
    ], function (err, docs) {
        //logger.log("err", err);


        let data = {
            new: 0,
            returning: 0
        };

        for (let doc of docs) {
            if (doc.count > 1)
                data.returning++;
            else
                data.new++;
        }

        //logger.log("docs", data);

        //let tmp = {newVsReturningUsers: docs};
        Updater.update(apiKey, {'historic.newVsReturningUsers': data});

    });
};


module.exports.avgPageLoadTime = function (apiKey) {

    Event.aggregate([
        {
            $group: {
                '_id': {url: '$eventStory.page.url', pageTitle: '$eventStory.page.pageTitle'},
                'avgLoadTime': {$avg: '$eventStory.page.loadTime'}
            }
        }
    ], function (err, docs) {
        //logger.log("err", err);
        //logger.log("docs", docs);

        //let tmp = {avgPageLoadTime: docs};
        //Updater.update(apiKey, {historic: tmp});
        Updater.update(apiKey, {'historic.avgPageLoadTime': docs});

    });
};


module.exports.referrals = function (apiKey) {

    Event.aggregate([
        {$group: {'_id': '$eventStory.referrer.referrerURL', 'count': {$sum: 1}}}
    ], function (err, docs) {
        //logger.log("err", err);
        ////logger.log("docs", docs);

        let data = {};

        for (let doc of docs) {
            if (!doc._id) {
                data.direct = doc.count;
            } else {
                data[doc._id] = doc.count;
            }
        }

        //logger.log("docs", data);

        //let tmp = {referrals: docs};
        //Updater.update(apiKey, {historic: tmp});
        Updater.update(apiKey, {'historic.referrals': data});
    });
};


module.exports.eventCount = function (apiKey) {
    Event.aggregate([
        {$group: {'_id': '$eventName', 'count': {$sum: 1}}}
    ], function (err, docs) {
        //logger.log("err", err);
        //logger.log("docs", docs);

        //let tmp = {eventCount: docs};
        //Updater.update(apiKey, {historic: tmp});
        Updater.update(apiKey, {'historic.eventCount': docs});
    });
};


module.exports.osSpectrum = function (apiKey) {

    Event.aggregate([
        {$group: {'_id': '$eventStory.technology.systemOS', 'count': {$sum: 1}}}
    ], function (err, docs) {
        //logger.log("err", err);
        //logger.log("docs", docs);

        //let tmp = {osSpectrum: docs};
        //Updater.update(apiKey, {historic: tmp});
        Updater.update(apiKey, {'historic.osSpectrum': docs});
    });
};


module.exports.languageSpectrum = function (apiKey) {

    Event.aggregate([
        {$group: {'_id': '$eventStory.technology.browserLanguage', 'count': {$sum: 1}}}
    ], function (err, docs) {
        //logger.log("err", err);
        //logger.log("docs", docs);

        //let tmp = {languageSpectrum: docs};
        //Updater.update(apiKey, {historic: tmp});
        Updater.update(apiKey, {'historic.languageSpectrum': docs});
    });
};


module.exports.mobileSpectrum = function (apiKey) {

    Event.aggregate([
        {$group: {'_id': '$eventStory.technology.isMobile', 'count': {$sum: 1}}}
    ], function (err, docs) {
        //logger.log("err", err);


        let data ={
            mobile: 0,
            desktop: 0
        };

        for(let doc of docs){
            if(doc._id){
                data.mobile += doc.count;
            }else{
                data.desktop += doc.count;
            }
        }

        //logger.log("docs", data);

        //let tmp = {mobileSpectrum: docs};
        //Updater.update(apiKey, {historic: tmp});
        Updater.update(apiKey, {'historic.mobileSpectrum': data});

    });
};


module.exports.geoSpectrum = function (apiKey) {

    Event.aggregate([
        {$group: {'_id': {'countryName' :'$eventStory.location.country_name', 'countryCode': '$eventStory.location.country_code'}, 'count': {$sum: 1}}}
    ], function (err, docs) {
        //logger.log("err", err);
        //logger.log("docs", docs);

        //let tmp = {geoSpectrum: docs};
        //Updater.update(apiKey, {historic: tmp});
        Updater.update(apiKey, {'historic.geoSpectrum': docs});
    });
};