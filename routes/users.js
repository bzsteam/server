let express = require('express');
let router = express.Router();

let response = require('../config/response');
let {User, ObjectId} = require('../models/user');
let rmw = require('../middlewares/required');
let userAuth = require('../middlewares/userAuth');
const {CODES} = require('../config/codes');

let encryptor = require('../config/encryptor');
let _ = require('underscore');
let logger = require('../logger');

/* GET userRoute listing. */
router.route('/')
    .get(function (req, res, next) {
        if (!req.query.email) {
            res.status(400).json(response(CODES.emailRq));
            return;
        }
        if (!req.query.password) {
            res.status(400).json(response(CODES.passRq));
            return;
        }

        let password = encryptor.generatePasswordHash(req.query.password);
        //let password = req.query.password;

        User.findOne({
            'email': req.query.email,
            'password': password
        }, 'name email active privilege dateCreated', function (err, docs) {
            if (err) {
                res.status(500).send(response(CODES.inError));
            }
            else {
                if (docs) {
                    if (true != docs.active) {
                        res.status(400).send(response(CODES.accNActive));
                    } else {
                        req.session.user = docs;
                        req.session.userSecurityKey = encryptor.cookieSecurity(docs._id);

                        let cookieLifeTime = 3600000 * 24 * 7;
                        req.session.cookie.expires = new Date(Date.now() + cookieLifeTime); //1 week
                        req.session.cookie.maxAge = cookieLifeTime;

                        res.cookie('userSecurityKey', encryptor.cookieSecurity(docs._id), {
                            maxAge: cookieLifeTime,
                            expires: cookieLifeTime
                        });

                        res.send(response(CODES.success, req.session));
                    }
                } else {
                    res.status(400).send(response(CODES.login));
                }
            }
        });


    })
    .post(rmw.nameRequired,
        rmw.emailRequired,
        rmw.passRequired,
        function (req, res, next) {
            //logger.log('req', req.body);
            let user = new User();
            user.name = req.body.name;
            user.email = req.body.email;

            if (req.body.confirmPassword !== req.body.password) {
                res.status(400).json(response(CODES.passMisM));
                return;
            }

            user.password = encryptor.generatePasswordHash(req.body.password);

            User.find({email: req.body.email}, function (err, docs) {
                if (docs.length) {
                    res.status(400).json(response(CODES.emailReg));
                } else {
                    user.save().then(function (data) {
                        res.json(response(CODES.success, _.pick(data, 'name', 'email', 'dateCreated')));
                    }, function (err) {
                        res.status(500).json(response(CODES.inError, err));
                    });
                }
            });
        });


router.route('/logout')
    .get(function (req, res, next) {
        req.session.destroy();
        res.send(response(CODES.success));
    });


router.route('/isLoggedIn')
    .get(userAuth, function (req, res, next) {
        //if(re)
        res.send(response(CODES.success, req.session.user));
    });

router.route('/:id')
    .get(userAuth, function (req, res, next) {
        User.findById(req.params.id, function (err, data) {
            if (err)
                res.status(400).json(response(CODES.error, err));
            else {
                res.json(response(CODES.success, data));
            }
        });
    });

module.exports = router;
