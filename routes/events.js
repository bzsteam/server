/**
 * Created by aditya on 07/04/17.
 */
let express = require('express');
let router = express.Router();

let response = require('../config/response');
let {User, ObjectId} = require('../models/user');
let {Event} = require('../models/event');
let {Aggregate} = require('../models/aggregate');
let rmw = require('../middlewares/required');
let injectorAuth = require('../middlewares/injectorAuth');
let userAuth = require('../middlewares/userAuth');
const {CODES} = require('../config/codes');

let encryptor = require('../config/encryptor');
let _ = require('underscore');
let freegeoip = require('node-freegeoip');
let logger = require('../logger');

freegeoip.maxLatency = 750;

/* GET userRoute listing. */
router.route('/')
    .get(userAuth, function(req, res){
        Aggregate.findOne({'apiKey': req.session.user._id.toString()}, function(err, docs){
            if (err) {
                res.status(400).json(response(CODES.inError, err));
            } else {
                res.json(response(CODES.success, docs));
            }
        });
    })
    .post(injectorAuth, function (req, res) {

        let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        ip = ip.split(':')[3];

        if(!ip || ip ==='127.0.0.1' || ip === 'localhost'){
            //logger.log('changing ip from:', ip);
            ip = "";
        }

        freegeoip.getLocation(ip, function(err, location) {
            if(err){
                res.status(500).json(response(CODES.locationAPIErr, err));
            }else{

                //logger.log(location);
                req.body.eventStory.location = location;
                let event = new Event(req.body);
                logger.logF("Event", event);
                event.save().then(function (data) {
                    res.json(response(CODES.success, event));
                }, function (err) {
                    res.status(500).json(response(CODES.inError, err));
                });
            }

        });



    });


module.exports = router;
