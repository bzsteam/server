let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let morganLogger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let cors = require('cors');
let compression = require('compression');

//for security
let helmet = require('helmet');
let session = require('express-session');
const MongoStore = require('connect-mongo')(session);


let app = module.exports = express();

let dbString = require('./config/database').dbString;
require('./middlewares/required');

let users = require('./routes/users');
let events = require('./routes/events');

//for security
app.use(compression());
app.use(helmet());
app.disable('x-powered-by');
app.use(session({
    secret: '2C44-4D44-WppQ38S',
    store: new MongoStore(
        {
            url: dbString,
            ttl: 7 * 24 * 60 * 60 // = 7 days. Default
        }
    ),
    resave: true,
    saveUninitialized: true,
    cookie: {
        path: '/',
        httpOnly: false,
        maxAge: 1000 * 60 * 24 * 7 // 24*7 hours
    }
}));


let corsOptions = {
    origin: true,
    credentials: true
};
app.use(cors(corsOptions));


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(morganLogger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));

app.use('/users', users);
app.use('/events', events);

require('./engine/index');

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    //res.render('error');
    res.send(err.message);
});

module.exports = app;
