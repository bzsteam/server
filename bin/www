#!/usr/bin/env node

/**
 * Module dependencies.
 */

let app = require('../app');
let debug = require('debug')('server:server');
let http = require('http');
let logger = require('../logger');


/**
 * Multiple clusters for speed
 */
const cluster = require('cluster');
/*const*/ numCPUs = require('os').cpus().length + 1;


/**
 * Get port from environment and store in Express.
 */

let port = normalizePort(process.env.PORT || '3000');
app.set('port', port);


/**
 * Cluster Logic
 */
if (cluster.isMaster) {
    logger.log(`Master ${process.pid} is running`);

    // Fork workers.
    //numCPUs = 1 + 1;
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        logger.log(`worker ${worker.process.pid} died`);
    });

    cluster.on('death', (worker, code, signal) => {
        logger.log(`worker ${worker.process.pid} died`);
    });
} else if(cluster.worker.id > 1){
    // Workers can share any TCP connection
    // In this case it is an HTTP server
    /**
     * Create HTTP server.
     */


    let server = http.createServer(app);

    /**
     * Listen on provided port, on all network interfaces.
     */
    server.listen(port);
    server.on('error', onError);
    server.on('listening', onListening);


    /**
     * Event listener for HTTP server "error" event.
     */

    function onError(error) {
        if (error.syscall !== 'listen') {
            throw error;
        }

        let bind = typeof port === 'string'
            ? 'Pipe ' + port
            : 'Port ' + port;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case 'EACCES':
                logger.error(bind + ' requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                logger.error(bind + ' is already in use');
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    /**
     * Event listener for HTTP server "listening" event.
     */

    function onListening() {
        let addr = server.address();
        let bind = typeof addr === 'string'
            ? 'pipe ' + addr
            : 'port ' + addr.port;
        debug('Listening on ' + bind);
        logger.info('Listening on ' + bind);
    }

    logger.log(`Worker:${cluster.worker.id} ${process.pid} started`);
}


/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    let port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

