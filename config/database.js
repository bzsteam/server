/**
 * Created by aditya on 05/03/17.
 */
let app = require('../app');

let mongoose = require('mongoose');
let logger = require('../logger');
// Use bluebird
mongoose.Promise = require('bluebird');

//logger.log("env",app.get('env'));
let dbString;
try {
    if(process.env['MONGO'])
        dbString = 'mongodb://@database:27017/ByZeroStats'; // connect to Docker database
    else
        dbString = 'mongodb://@127.0.0.1:27017/ByZeroStats'; // connect to Local database

    mongoose.connect(dbString);

} catch (err) {
    logger.log(err);
}


module.exports = mongoose;
module.exports.dbString = dbString;