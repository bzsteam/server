/**
 * Created by aditya on 05/03/17.
 */

let passwordHash = require('password-hash');
let md5 = require('md5');

const securitySalt1 = "BZS123";
const securitySalt2 = "BZS123";

let generatePasswordHash = function (password) {
    //return passwordHash.generate(password);
    return password;
};

//not working coz of above pass-hash
let checkPassword = function (password, hashedPassword) {
    return passwordHash.verify(password, hashedPassword);
};


let cookieSecurity = function(key){
    return md5(securitySalt1 + key + securitySalt2);
};

module.exports.generatePasswordHash = generatePasswordHash;
//module.exports.checkPassword = checkPassword;
module.exports.md5 = md5;
module.exports.cookieSecurity = cookieSecurity;
