/**
 * Created by aditya on 05/03/17.
 */

let mongoose = require('../config/database');
let ObjectId = require('../config/database').Types.ObjectId;
let Schema = mongoose.Schema;

let UserSchema = new Schema({
    name: String,
    email: String,
    password: String,
    active: {type: Boolean, default: true},
    privilege: {type: Number, default: 0},
    dateCreated: {type: Date, default: Date.now()}
});

module.exports.User = mongoose.model('Users', UserSchema);
module.exports.ObjectId = ObjectId;
