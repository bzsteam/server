/**
 * Created by aditya on 05/03/17.
 */

let mongoose = require('../config/database');
let ObjectId = require('../config/database').Types.ObjectId;
let Schema = mongoose.Schema;

let AggregateSchema = new Schema({
    apiKey: String,
    live: {
        countLiveUser: {type: Schema.Types.Mixed}
    },
    historic: {
        usersOverTime: {type: Schema.Types.Mixed},
        pageVisitCount: {type: Schema.Types.Mixed},
        newVsReturningUsers: {type: Schema.Types.Mixed},
        avgPageLoadTime: {type: Schema.Types.Mixed},
        referrals: {type: Schema.Types.Mixed},
        eventCount: {type: Schema.Types.Mixed},
        osSpectrum: {type: Schema.Types.Mixed},
        languageSpectrum: {type: Schema.Types.Mixed},
        mobileSpectrum: {type: Schema.Types.Mixed},
        geoSpectrum: {type: Schema.Types.Mixed}
    },
    dateCreated: {type: Date, default: Date.now()}
});

module.exports.Aggregate = mongoose.model('Aggregate', AggregateSchema);
module.exports.ObjectId = ObjectId;
