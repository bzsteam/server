/**
 * Created by aditya on 05/03/17.
 */

let mongoose = require('../config/database');
let ObjectId = require('../config/database').Types.ObjectId;
let Schema = mongoose.Schema;

let EventStorySchema = new Schema({
    technology: {
        browserName: String,
        browserVersion: String,
        browserLanguage: String,
        systemOS: String,
        viewHeight: Number,
        viewWidth: Number,
        screenHeight: Number,
        screenWidth: Number,
        colorDepth: Number,
        cookieEnabled: Boolean,
        flashVersion: String,
        quickTimeEnabled: Boolean,
        isMobile: Boolean,
        javaEnabled: Boolean,
        userAgent: String,
    },
    page: {
        url: String,
        pageTitle: String,
        loadTime: Number,
        pageVisitTimeStamp: Date
    },
    referrer: {
        referrerURL: String
    },
    guid: {
        guid: String
    },
    location: {
        ip: String,
        country_code: String,
        country_name: String,
        region_code: String,
        region_name: String,
        city: String,
        zip_code: String,
        time_zone: String,
        latitude: Number,
        longitude: Number,
        metro_code: Number
    }
});

let EventSchema = new Schema({
    appName: String,
    apiKey: String,
    apiVersion: String,
    eventName: String,
    eventData: {type: Schema.Types.Mixed, default: {}},
    eventStory: EventStorySchema,
    dateCreated: {type: Date, default: Date.now()}
});

module.exports.Event = mongoose.model('Events', EventSchema);
module.exports.ObjectId = ObjectId;
