/**
 * Created by aditya on 08/04/17.
 */


let fs = require("fs");
let all = fs.createWriteStream('./logs/all.log', { flags: 'a' })
    , log = fs.createWriteStream('./logs/log.log', { flags: 'a' })
    , info = fs.createWriteStream('./logs/info.log', { flags: 'a' })
    , warn = fs.createWriteStream('./logs/warn.log', { flags: 'a' })
    , error = fs.createWriteStream('./logs/error.log', { flags: 'a' });
const util = require('util');


module.exports.log = function (...text) {
    console.log('[#LOG] ', ...text);
    //console.log('test', util.format.apply(null, text));
    log.write('[#LOG] ' + util.format.apply(null, text) + '\n');
    all.write('[#LOG] ' + util.format.apply(null, text) + '\n');
};
module.exports.info = function (...text) {
    console.info('[#INFO] ', ...text);
    info.write('[#INFO] ' + util.format.apply(null, text) + '\n');
    all.write('[#INFO] ' + util.format.apply(null, text) + '\n');
};
module.exports.warn = function (...text) {
    console.warn('[#INFO] ', ...text);
    warn.write('[#INFO] ' + util.format.apply(null, text) + '\n');
    all.write('[#INFO] ' + util.format.apply(null, text) + '\n');
};
module.exports.error = function (...text) {
    console.error('[#INFO] ', ...text);
    error.write('[#INFO] ' + util.format.apply(null, text) + '\n');
    all.write('[#INFO] ' + util.format.apply(null, text) + '\n');
};

module.exports.logF = function (...text) {
    log.write('[#LOG] ' + util.format.apply(null, text) + '\n');
    all.write('[#LOG] ' + util.format.apply(null, text) + '\n');
};



