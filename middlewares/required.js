/**
 * Created by aditya on 05/03/17.
 */

let express = require('express');

let response = require('../config/response');
let {CODES} = require('../config/codes');

let nameRequired = function (req, res, next) {
    if (!req.body.name) {
        res.status(400).json(response(CODES.nameRq));
    } else {
        req.body.name = req.body.name.trim().toLowerCase();
        next()
    }
};
let emailRequired = function (req, res, next) {
    if (!req.body.email) {
        res.status(400).json(response(CODES.emailRq));
    } else {
        req.body.email = req.body.email.trim();
        next()
    }
};
let passRequired = function (req, res, next) {
    if (!req.body.password) {
        res.status(400).json(response(CODES.passRq));
    } else if (req.body.password.length < 6) {
        res.status(400) .json(response(CODES.passLen));
    }
    else {
        next()
    }
};

let addressRequired = function (req, res, next) {
    if (!req.body.address) {
        res.status(400).json(response(CODES.addRq));
    } else {
        req.body.address = req.body.address.trim();
        next()
    }
};


let landmarkRequired = function (req, res, next) {
    if (!req.body.landmark) {
        res.status(400).json(response(CODES.landmarkRq));
    } else {
        req.body.landmark = req.body.landmark.trim();
        next()
    }
};


let mobNoRequired = function (req, res, next) {
    if (!req.body.phone) {
        res.status(400).json(response(CODES.phoneRq));
    } else {
        req.body.phone = req.body.phone.trim();
        next()
    }
};



module.exports.nameRequired = nameRequired;
module.exports.emailRequired = emailRequired;
module.exports.passRequired = passRequired;
module.exports.addressRequired = addressRequired;
module.exports.landmarkRequired = landmarkRequired;
module.exports.mobNoRequired = mobNoRequired;