/**
 * Created by aditya on 05/03/17.
 */

let response = require('../config/response');
const {CODES} = require('../config/codes');

let userAuth = function (req, res, next) {
    // Authentication and Authorization Middleware
    if (req.session && req.session.user) {
        if (req.session.userSecurityKey === req.cookies.userSecurityKey)
            return next();
    }
    //return res.status(401).send(response(CODES.session, req.cookies.adminSecurityKey, req.session.adminSecurityKey));
    return res.status(401).send(response(CODES.session, req.session.user));
};


module.exports = userAuth;