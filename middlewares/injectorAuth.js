/**
 * Created by aditya on 07/04/17.
 */


let response = require('../config/response');
const {CODES} = require('../config/codes');
let {User, ObjectId} = require('../models/user');
let logger = require('../logger');

let injectorAuth = function (req, res, next) {

    logger.log("API KEY:",req.body.apiKey);

    if(!ObjectId.isValid(req.body.apiKey)){
        return res.status(401).send(response(CODES.injectorAuthInvalid));
    }

    User.count({ '_id': new ObjectId(req.body.apiKey) }, function (err, count) {
        if (err || count < 1)
            return res.status(401).send(response(CODES.injectorAuthInvalid, count));
        else
            return next();
    });


};


module.exports = injectorAuth;